* [ENGLISH VERSION](#markdown-header-first-steps-with-pico-8)
* [VERSIÓN ESPAÑOLA](#markdown-header-primeros-pasos-con-pico-8)

# FIRST STEPS WITH PICO-8 #
## SCREENS ##
### CODE ###
![2017-03-22 20_37_07-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/1783780172-2017-03-22%2020_37_07-pico-8.png)
### SPRITE STUDIO ###
![2017-03-22 20_33_40-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/1452022026-2017-03-22%2020_33_40-pico-8.png)
### MAP STUDIO ###
![2017-03-22 20_34_00-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/2703703457-2017-03-22%2020_34_00-pico-8.png)
### SFX STUDIO ###
![2017-03-22 20_34_23-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/1366498262-2017-03-22%2020_34_23-pico-8.png)
### MUSIC STUDIO ###
![2017-03-22 20_35_01-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/387415392-2017-03-22%2020_35_01-pico-8.png)
### TERMINAL ###
![2017-03-22 20_39_19-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/2468868303-2017-03-22%2020_39_19-pico-8.png)

### MAIN FUNCTIONS ###

```
#!lua
_init()

```
_init is called once, when the game starts, but you can call it anywhere, anytime. perfect for a reset switch


```
#!lua
_update()

```
_update is called 30 times per second (30fps, rings any bell?)


```
#!lua
_draw()

```
_draw is called at the end of every _update call

### EXAMPLE ###
Skeleton.p8: fresh start with a few printed lines to see how many times is every function called

### USEFUL SHORTCUTS ###
* F7: Save screenshot as game cover
* F8: Frame to start recording a GIF
* F9: Save GIF to your desktop (8 seconds from the last time you pressed F8)

# PRIMEROS PASOS CON PICO-8 #
## PANTALLAS ##
### CÓDIGO ###
![2017-03-22 20_37_07-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/1783780172-2017-03-22%2020_37_07-pico-8.png)
### ESTUDIO DE SPRITES ###
![2017-03-22 20_33_40-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/1452022026-2017-03-22%2020_33_40-pico-8.png)
### ESTUDIO DE MAPAS ###
![2017-03-22 20_34_00-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/2703703457-2017-03-22%2020_34_00-pico-8.png)
### ESTUDIO DE EFECTOS DE SONIDO ###
![2017-03-22 20_34_23-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/1366498262-2017-03-22%2020_34_23-pico-8.png)
### ESTUDIO DE BANDA SONORA ###
![2017-03-22 20_35_01-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/387415392-2017-03-22%2020_35_01-pico-8.png)
### TERMINAL ###
![2017-03-22 20_39_19-pico-8.png](https://bitbucket.org/repo/LoRdMkd/images/2468868303-2017-03-22%2020_39_19-pico-8.png)

### FUNCIONES PRINCIPALES ###

```
#!lua
_init()

```
_init se ejecuta una vez, cuando el juego comienza, pero puedes llamarla donde y cuando quieras. perfecto para un boton de reset


```
#!lua
_update()

```
_update se ejecuta 30 veces por segundo (30fps, te suena?)


```
#!lua
_draw()

```
_draw se ejecuta al final de cada llamada a _update

### EXAMPLE ###
Skeleton.p8: Inicio desde cero, con un par de líneas impresas para que veas cuantas veces se llama a una función

### USEFUL SHORTCUTS ###
* F7: Guarda una captura de pantalla como carátula del juego
* F8: Fotograma desde el que guardar un GIF
* F9: Guardar el GIF en el escritorio (8 segundos desde la última vez que pulsaste F8)